package com.ikc.demo.publicspring.controller;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(controllers = ServiceController.class)
public class ServiceControllerTests {
    @Autowired
    private MockMvc mvc;

    @Test
    public void test1() throws Exception {
        String str = "service";

        mvc.perform(get("/service"))
                .andExpect(status().isOk())
                .andExpect(content().string(str));
    }
}