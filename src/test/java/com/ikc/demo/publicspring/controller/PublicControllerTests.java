package com.ikc.demo.publicspring.controller;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(controllers = PublicController.class)
public class PublicControllerTests {
    @Autowired
    private MockMvc mvc;

    @Test
    public void test1() throws Exception {
        String str = "public";

        mvc.perform(get("/public"))
                .andExpect(status().isOk())
                .andExpect(content().string(str));
    }
}
