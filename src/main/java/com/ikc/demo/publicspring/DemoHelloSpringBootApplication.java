package com.ikc.demo.publicspring;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoHelloSpringBootApplication {
	public static void main(String[] args) {
		SpringApplication.run(DemoHelloSpringBootApplication.class, args);
	}
}
