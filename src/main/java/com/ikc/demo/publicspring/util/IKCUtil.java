package com.ikc.demo.publicspring.util;

public class IKCUtil {
    private IKCUtil() {

    }

    public static int sum(int a, int b) {
        return a + b;
    }

    public static int min(int a, int b) {
        return a - b;
    }
}
