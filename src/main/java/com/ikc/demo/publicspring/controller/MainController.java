package com.ikc.demo.publicspring.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MainController {
    @GetMapping("/")
    public ResponseEntity<String> hello() {
        String msg = "Public Spring Boot";
        return ResponseEntity.ok(msg);
    }
}


