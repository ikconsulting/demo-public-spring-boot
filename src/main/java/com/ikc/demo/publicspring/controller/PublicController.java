package com.ikc.demo.publicspring.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class PublicController {
    @GetMapping("/public")
    public ResponseEntity<String> hello() {
        String msg = "public";
        return ResponseEntity.ok(msg);
    }
}
