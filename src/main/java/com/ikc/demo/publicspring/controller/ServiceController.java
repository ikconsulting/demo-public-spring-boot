package com.ikc.demo.publicspring.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ServiceController {
    @GetMapping("/service")
    public ResponseEntity<String> hello() {
        String msg = "service";
        return ResponseEntity.ok(msg);
    }
}
