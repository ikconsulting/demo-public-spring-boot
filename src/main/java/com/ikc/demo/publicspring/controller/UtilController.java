package com.ikc.demo.publicspring.controller;

import com.ikc.demo.publicspring.util.IKCUtil;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UtilController {

    @GetMapping("/util/sum")
    public ResponseEntity<String> sum() {
        int sum = IKCUtil.sum(5, 5);
        return ResponseEntity.ok("" + sum);
    }

    @GetMapping("/util/min")
    public ResponseEntity<String> min() {
        int min = IKCUtil.min(15, 5);
        return ResponseEntity.ok("" + min);
    }
}
